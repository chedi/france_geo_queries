from django.contrib.gis.db import models


class Region(models.Model):
    nom  = models.TextField()
    code = models.TextField()

    class Meta:
        unique_together = ("nom", "code")

    def __unicode__(self):
        return self.nom


class Departement(models.Model):
    nom     = models.TextField()
    code    = models.TextField()
    region  = models.ForeignKey(Region)
    geom    = models.MultiPolygonField(srid=2154)
    objects = models.GeoManager()

    def __unicode__(self):
        return self.nom


class Arrondissement(models.Model):
    code        = models.TextField()
    region      = models.ForeignKey(Region)
    departement = models.ForeignKey(Departement)
    geom        = models.MultiPolygonField(srid=2154)
    objects     = models.GeoManager()

    def __unicode__(self):
        return u'{0} ({1})'.format(self.code, self.region)


class Canton(models.Model):
    code           = models.TextField()
    arrondissement = models.ForeignKey(Arrondissement)
    geom           = models.MultiPolygonField(srid=2154)
    objects        = models.GeoManager()

    def __unicode__(self):
        return u'{0} ({1})'.format(self.code, self.region)


class Commune(models.Model):
    nom        = models.TextField()
    zip        = models.TextField()
    code       = models.TextField()
    insee      = models.TextField()
    statut     = models.TextField()
    canton     = models.ForeignKey(Canton)
    population = models.FloatField()
    superficie = models.IntegerField()
    geom       = models.MultiPolygonField(srid=2154)
    objects    = models.GeoManager()

    def __unicode__(self):
        return self.nom


class Auchan(models.Model):
    store_url    = models.TextField()
    store_name   = models.TextField()
    store_rank   = models.IntegerField()
    store_adress = models.TextField()
    geom         = models.PointField(srid=4326)
    objects      = models.GeoManager()


class Carrefour(models.Model):
    nom      = models.TextField()
    zip      = models.TextField()
    tel      = models.TextField()
    addresse = models.TextField()
    geom     = models.PointField(srid=4326)
    objects  = models.GeoManager()


class CarrefourDrive(models.Model):
    name    = models.TextField()
    unknown = models.TextField()
    geom    = models.PointField(srid=4326)
    objects = models.GeoManager()


class CarrefourMarket(models.Model):
    nom      = models.TextField()
    zip      = models.TextField()
    tel      = models.TextField()
    addresse = models.TextField()
    geom     = models.PointField(srid=4326)
    objects  = models.GeoManager()


class Monoprix(models.Model):
    nom      = models.TextField()
    zip      = models.TextField()
    ctm      = models.TextField()
    fax      = models.TextField()
    ville    = models.TextField()
    telphone = models.TextField()
    addresse = models.TextField()
    geom     = models.PointField(srid=4326)
    objects  = models.GeoManager()
