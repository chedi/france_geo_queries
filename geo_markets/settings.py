import os

DEBUG          = True
TEMPLATE_DEBUG = DEBUG

PROJECT_ROOT = os.path.abspath(os.path.dirname(__name__))

ADMINS = (
    ('chedi toueiti', 'chedi.toueiti@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE'  : 'django.contrib.gis.db.backends.postgis',
        'NAME'    : 'geo_markets',
        'USER'    : 'postgres',
        'PASSWORD': '',
        'HOST'    : 'localhost',
        'PORT'    : '5432',
    }
}

USE_TZ        = True
SITE_ID       = 1
USE_I18N      = True
USE_L10N      = True
TIME_ZONE     = 'Africa/Tunis'
LANGUAGE_CODE = 'fr-fr'

MEDIA_URL   = '/media/'
STATIC_URL  = '/static/'
MEDIA_ROOT  = os.path.abspath(os.path.join(PROJECT_ROOT, "./media" ))
GEOIP_PATH  = os.path.abspath(os.path.join(PROJECT_ROOT, "./geoip" ))
STATIC_ROOT = os.path.abspath(os.path.join(PROJECT_ROOT, "./static"))

STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = '*go1do$bv+bpe8e*djns%8tyh*3ui-dlx0b^6&70wpn%v$yjwn'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.csrf',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
)

ROOT_URLCONF     = 'geo_markets.urls'
WSGI_APPLICATION = 'geo_markets.wsgi.application'

TEMPLATE_DIRS = (
    os.path.abspath(os.path.join(PROJECT_ROOT, 'templates')),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',

    'suit',
    'south',
    'gunicorn',
    'djgeojson',
    'debug_toolbar',
    'rest_framework',

    'django.contrib.admin',

    'markets',
    'france_geo_data',
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

SUIT_CONFIG = {
    'VERSION' : "0.1",

    'ADMIN_NAME': 'GNV admin',
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    'SEARCH_URL': '/admin/auth/user/',
    'MENU_ICONS': {
        'sites': 'icon-leaf',
        'auth': 'icon-lock',
    },
    'MENU_OPEN_FIRST_CHILD': True,

    'MENU': (
        {'app': 'auth', 'label': "Users & Groups"},
        '-',
        {'label': 'Support', 'icon': 'icon-question-sign', 'url': '/support/'},
    ),

    'LIST_PER_PAGE': 30
}

DEBUG_TOOLBAR_CONFIG = {
    'HIDE_DJANGO_SQL'    : False,
    'ENABLE_STACKTRACES' : True,
    'INTERCEPT_REDIRECTS': False,
}

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    #'debug_toolbar.panels.profiling.ProfilingDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.cache.CacheDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)

INTERNAL_IPS = ('127.0.0.1',)


def show_toolbar(request):
    return False

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': show_toolbar,
    'INTERCEPT_REDIRECTS': False,
}
